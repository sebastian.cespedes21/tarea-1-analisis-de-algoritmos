#include <iostream>
#include <unistd.h>
using namespace std;

int frecuenciaAbs(int a, int [],int lar);
int frecAbsAcum(int a, int [],int lar);
int frecRelAcum(int abs, int datos);
	
int main(){
cout << "Cuantos elementos se van a analizar?\n";
int n, i;
i =0;
cin >> n;
int numeros[n];
cout << "Introduzca los numeros\n";
while (i<n){
	cin >> numeros[i];
	i += 1;
	}
for (int i=0;i<n;i++){
	cout << "Frecuencia absoluta de: " << numeros[i] <<"\n";
	cout << frecuenciaAbs(numeros[i], numeros, n) << "\n";
	cout << "Frecuencia absoluta acumulada de: " << numeros[i] << "\n";
	int frecAbs = frecAbsAcum(numeros[i], numeros, n);
	cout << frecAbs << "\n";
	cout << "Frecuencia absoluta relativa de: " << numeros[i] << "\n";
	cout << frecRelAcum(frecAbs, n) << "\n" << "\n" << "\n" << "\n";
	usleep(999999);
	}
return 0;
}	

int frecuenciaAbs(int a, int l[],int lar){
        int contador =0;
        for (int i=0;i<lar;i++){
                if (l[i]==a){
                        contador+=1;
                }
	}
	return contador;
}

int frecAbsAcum(int a, int l[], int lar){
	int contador = 0;
	for (int i=0;i<lar;i++){
		if (l[i]<=a){
			contador+=1;
		}
	}
	return contador;
}

int frecRelAcum(int abs, int datos){
	return abs/datos;
}
