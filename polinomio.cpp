#include <iostream>
using namespace std;

int main(){ 
int polinomio[100], maxExp, i=0; 
cout << "Digite el mayor exponente\n"; 
cin >> maxExp; 
cout << "Digite el polinomio\n"; 
i = maxExp; 
while (i>-1){
	cin >> polinomio[i]; 
	i -= 1;
	}
cout << "{"; 
i = 0;
while (i<=maxExp){ 
	cout << polinomio[i] << ",";
	i += 1;
	}
cout << "\b}"; 
return 0;
}

